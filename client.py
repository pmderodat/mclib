# -*- coding: utf-8 -*-

import collections
from Queue import Queue
import socket
import StringIO
import threading

import map
from packet import *



class Client(object):
    '''
    Wrap basic operations on a connection to a server.
    '''

    PROTOCOL_VERSION = 23

    def __init__(self, username):
        self.username = username
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.receiving_thread = None
        self.sending_thread = None
        self.sending_queue = Queue(maxsize=100)
        self.map = None
        self.spawned = False
        self.spawn_position = None
        self.player_coroutine = None

        # Initialize packet handlers: the default one does nothing.
        self.handlers = collections.defaultdict(lambda: (lambda packet: None))
        self.handlers.update({
            KeepAlive.packet_id: self.handle_keepalive,
            SpawnPosition.packet_id: self.handle_spawnposition,
            PlayerPosition.packet_id: self.handle_playerposition,
            PlayerPositionLookServer.packet_id: self.handle_playerpositionlook,
            PreChunk.packet_id: self.handle_prechunk,
            MapChunk.packet_id: self.handle_mapchunk,
            MultiBlockChange.packet_id: self.handle_multiblockchange,
            BlockChange.packet_id: self.handle_blockchange,
            ChatMessage.packet_id: self.handle_chat,
        })
        self.connected = False

    def connect(self, address):
        try:
            self.socket.connect(address)
        except socket.error, e:
            self.on_disconnection(e)

        # Start handshake
        self.send_packet(HandshakeClient(
            username=self.username
        ))
        response = self.recv_packet()
        self.expect_packet(response, HandshakeServer)
        self.connection_hash = response.connection_hash

        # Send the login request
        self.send_packet(LoginRequestClient(
            protocol_version=self.PROTOCOL_VERSION,
            username=self.username
        ))
        response = self.recv_packet()
        self.expect_packet(response, LoginRequestServer)

        # Login is successful, start network coroutines and initialize data.
        self.connected = True
        self.receiving_thread = threading.Thread(target=self.receive_forever)
        self.sending_thread = threading.Thread(target=self.send_forever)
        self.receiving_thread.daemon = True
        self.sending_thread.daemon = True
        self.map = map.Map()
        self.player = map.Player(self.map)
        self.receiving_thread.start()
        self.sending_thread.start()

    def expect_packet(self, packet, expected):
        if isinstance(packet, Disconnect):
            raise ConnectionClosed(packet.reason)
        if not isinstance(packet, expected):
            raise ProtocolError(
                "Expected a %s packet, but server sent a %s one" % (
                    expected.__name__,
                    packet.__class__.__name__
                )
            )

    def recv_packet(self):
        try:
            packet = Packet.parse_server(self.socket)
            logging.debug('Packet received: %s', packet)
            return packet
        except socket.error, e:
            logging.debug('Packet not received: %s', e)
            self.on_disconnection(e)
            raise

    def receive_forever(self):
        while True:
            packet = self.recv_packet()
            self.handlers[packet.packet_id](packet)

    def send_packet(self, packet):
        try:
            self.socket.sendall(packet.serialize())
            logging.debug('Packet sent: %s', packet)
        except socket.error, e:
            logging.debug('Packet not sent: %s', packet)
            self.on_disconnection(e)
            raise

    def put_packet(self, packet):
        self.sending_queue.put(packet)

    def send_forever(self):
        while True:
            packet = self.sending_queue.get()
            self.send_packet(packet)

    def on_disconnection(self, exception):
        '''
        Simple hook, can be overridden by subclasses. 'exception' contains the
        exception that caused the disconnection, or None if the disconnection
        was expected.
        '''
        self.connected = False

    def close(self):
        self.socket.close()
        self.on_disconnection(None)

    def handle_keepalive(self, packet):
        # The server tests the connection sending a Keep Alive packet.
        # We must send back the same packet.
        self.put_packet(packet)

    def handle_chat(self, packet):
        pass

    def handle_spawnposition(self, packet):
        self.spawn_position = (packet.x, packet.y, packet.z)
        if not self.spawned:
            self.player.set_position(packet.x, packet.y, packet.z)

    def handle_playerposition(self, packet):
        self.player.set_position(packet.x, packet.y, packet.z, packet.stance)
        self.spawned = True

    def handle_playerpositionlook(self, packet):
        self.player.set_position(packet.x, packet.y, packet.z, packet.stance)
        self.player.set_look(packet.yaw, packet.pitch)
        if not self.spawned:
            self.spawned = True
        # Acknowledge the server that we agree with him, not to be kicked.
        self.put_packet(self.player.get_position_packet())

    def handle_prechunk(self, packet):
        if packet.mode:
            self.map.put((packet.x, packet.z), map.Chunk())
        else:
            self.map.remove((packet.x, packet.z))

    def handle_mapchunk(self, packet):
        chunk_x = packet.x >> 4
        chunk_y = packet.y >> 7
        chunk_z = packet.z >> 4
        chunk = self.map.get((chunk_x, chunk_z))
        start_x = packet.x & 15
        start_y = packet.y & 127
        start_z = packet.z & 15
        count = (packet.size_x + 1) * (packet.size_y + 1) * (packet.size_z + 1)

        data = StringIO.StringIO(packet.data)
        byte_format = '>' + 'B' * count
        half_format = '>' + 'B' * (count / 2)
        block_types = list(struct.unpack(
            byte_format, data.read(count)
        ))
        metadata = list(struct.unpack(
            half_format, data.read(count / 2)
        ))
        light = list(struct.unpack(
            half_format, data.read(count / 2)
        ))
        sky_light = list(struct.unpack(
            half_format, data.read(count / 2)
        ))

        def get_half(array, is_first_half):
            return (
                array[0] & 0xF
                if is_first_half else
                array.pop(0) >> 4
            )

        first_half = True
        for i in xrange(count):
            chunk.put(
                (
                    start_x + i >> 11,
                    start_y + i & 0x7f,
                    start_z + (i & 0x780) >> 7
                ),
                map.Block(
                    block_types.pop(0),
                    get_half(metadata, first_half),
                    get_half(light, first_half),
                    get_half(sky_light, first_half),
                )
            )
            first_half = not first_half

    def handle_multiblockchange(self, packet):
        chunk = self.map.get((packet.x, packet.z))
        if chunk is None:
            return
        for coordinates, type, metadata in zip(*packet.data):
            coordinates = (
                coordinates & 0xFF,
                (coordinates >> 8) & 0xFF,
                coordinates >> 12
            )
            chunk.put(
                coordinates,
                # TODO: handle metadata.
                chunk.get(coordinates)._replace(type=type)
            )

    def handle_blockchange(self, packet):
        chunk = self.map.get((packet.x >> 4, packet.z >> 4))
        if chunk is None:
            return
        coordinates = (packet.x & 0xF, packet.y, packet.z & 0xF)
        chunk.put(
            coordinates,
            # TODO: handle metadata.
            chunk.get(coordinates)._replace(type=packet.block_type)
        )
