# -*- coding: utf-8 -*-

import collections
import logging
import socket
import struct
import zlib



INVALID_BED = 0
BEGIN_RAINING = 1
END_RAINING = 2
CHANGE_GAME_MODE = 3
ENTER_CREDITS = 4

NO_ANIMATION=0
SWING_ARM=1
DAMAGE_ANIMATION=2
LEAVE_BED=3
EAT_FOOD=5
UNKNOWN_ANIMATION=102
CROUCH=104
UNCROUCH=105

ACTION_CROUCH = 1
ACTION_UNCROUCH = 2
ACTION_LEAVE_BED = 3
ACTION_START_SPRINTING = 4
ACTION_STOP_SPRINTING = 5

ENTITY_HURT=2
ENTITY_DEAD=3
WOLF_TAMING=6
WOLF_TAMED=7
WOLF_SHAKING=8
SELF_EATING_ACCEPTED=9

FLINT_AND_STEEL = 0x103
BOW = 0x105
FISHING_ROD = 0x15A
SHEARS = 0x167

WOOD_SWORD = 0x10C
WOOD_SHOVEL = 0x10D
WOOD_PICKAXE = 0x10E
WOOD_AXE = 0x10F
WOOD_HOE = 0x122

STONE_SWORD = 0x110
STONE_SHOVEL = 0x111
STONE_PICKAXE = 0x112
STONE_AXE = 0x113
STONE_HOE = 0x123

IRON_SWORD = 0x10B
IRON_SHOVEL = 0x100
IRON_PICKAXE = 0x101
IRON_AXE = 0x102
IRON_HOE = 0x124

DIAMOND_SWORD = 0x114
DIAMOND_SHOVEL = 0x115
DIAMOND_PICKAXE = 0x116
DIAMOND_AXE = 0x117
DIAMOND_HOE = 0x125

GOLD_SWORD = 0x11B
GOLD_SHOVEL = 0x11C
GOLD_PICKAXE = 0x11D
GOLD_AXE = 0x11E
GOLD_HOE = 0x126

enchantable_items = set((
    FLINT_AND_STEEL,
    BOW,
    FISHING_ROD,
    SHEARS,
    WOOD_SWORD, WOOD_SHOVEL, WOOD_PICKAXE, WOOD_AXE, WOOD_HOE,
    STONE_SWORD, STONE_SHOVEL, STONE_PICKAXE, STONE_AXE, STONE_HOE,
    IRON_SWORD, IRON_SHOVEL, IRON_PICKAXE, IRON_AXE, IRON_HOE,
    DIAMOND_SWORD, DIAMOND_SHOVEL, DIAMOND_PICKAXE, DIAMOND_AXE, DIAMOND_HOE,
    GOLD_SWORD, GOLD_SHOVEL, GOLD_PICKAXE, GOLD_AXE, GOLD_HOE,
))



class ConnectionClosed(socket.error):
    pass

class ProtocolError(Exception):
    pass



def recvall(socket, size):
    '''
    Read *exactly* "size" bytes from the socket and return them. Raise a
    ConnectionClosed exception or a socket.error otherwise.
    '''
    buffer = ''
    while len(buffer) < size:
        bytes_received = socket.recv(size - len(buffer))
        if len(bytes_received) == 0:
            raise ConnectionClosed()
        buffer += bytes_received
    return buffer



class BaseType(object):
    # Subclasses must set this to a suitable struct format/data size or to
    # redefine the parse and serialize methods.
    format = None
    size = None

    @classmethod
    def parse(cls, socket):
        return struct.unpack(cls.format, recvall(socket, cls.size))[0]

    @classmethod
    def serialize(cls, value):
        return struct.pack(cls.format, value)

class Byte(BaseType):
    format = '>b'
    size = 1

class UnsignedByte(BaseType):
    format = '>B'
    size = 1

class Short(BaseType):
    format = '>h'
    size = 2

class Int(BaseType):
    format = '>i'
    size = 4

class Long(BaseType):
    format = '>q'
    size = 8

class Float(BaseType):
    format = '>f'
    size = 4

class Double(BaseType):
    format = '>d'
    size = 8

class String(BaseType):
    @classmethod
    def parse(cls, socket):
        length = Short.parse(socket)
        return recvall(socket, 2 * length).decode('utf_16_be')

    @classmethod
    def serialize(cls, value):
        return Short.serialize(len(value)) + value.encode('utf_16_be')

class Bool(BaseType):
    @classmethod
    def parse(cls, socket):
        return recvall(socket, 1) != '\x00'

    @classmethod
    def serialize(cls, value):
        return '\x01' if value else '\x00'

def Array(index_type, value_type):
    class _Array(BaseType):
        @classmethod
        def parse(cls, socket):
            length = index_type.parse(socket)
            return [value_type.parse(socket) for i in xrange(length)]

        @classmethod
        def serialize(cls, value):
            return (
                index_type.serialize(len(value)) +
                ''.join(value_type.serialize(element) for element in value)
            )

    return _Array

def MultiArray(index_type, value_types):
    '''
    Return a type that concatenates equally-sized arrays. 'index_type' is the
    type that contains the length of the arrays and 'value_types' is a list of
    item types in arrays.

    For instance, with
      - index_type=Short
      - value_types=[('item_type', Byte), ('item_count', Byte)]
    One gets:
        <short arrays length>
        <array of bytes>
        <array of integers>
    '''

    assert len(value_types) > 0
    _Arrays = collections.namedtuple(
        '_Arrays',
        (name for name, _ in value_types)
    )

    class _Array(BaseType):
        @classmethod
        def parse(cls, socket):
            length = index_type.parse(socket)
            return _Arrays(*(
                [value_type.parse(socket) for i in xrange(length)]
                for _, value_type in value_types
            ))

        @classmethod
        def serialize(cls, value):
            return (
                index_type.serialize(len(value[0])) +
                ''.join(itertools.chain(
                    (value_type.serialize(element) for element in sub_value)
                    for sub_value, value_type in zip(value, value_types)
                ))
            )

    return _Array

class Slot(BaseType):
    @classmethod
    def parse(cls, socket):
        item_id = Short.parse(socket)
        if item_id == -1:
            return None
        item_count = Byte.parse(socket)
        data = Short.parse(socket)
        # TODO: fill a proper data structure to be returned
        result = None
        if item_id in enchantable_items:
            enchanted_data_length = Short.parse(socket)
            enchanted_data = recvall(socket, enchanted_data_length)
            # TODO: extract enchantment data
            return result

    @classmethod
    def serialize(cls, value):
        pass

class Metadata(BaseType):

    @classmethod
    def parse_entity(cls, socket):
        return {
            'id': Short.parse(socket),
            'count': Byte.parse(socket),
            'damage': Short.pares(socket),
        }

    @classmethod
    def parse_triplet(cls, socket):
        return [cls.parse_int(socket) for i in xrange(3)]

    @classmethod
    def parse(cls, socket):
        metadata = {}
        type_dispatch = {
            0: Byte.parse,
            1: Short.parse,
            2: Int.parse,
            3: Float.parse,
            4: String.parse,
            # TODO: give a better name to the two following methods:
            5: cls.parse_entity,
            6: cls.parse_triplet,
        }
        while True:
            byte = UnsignedByte.parse(socket)
            if byte == 127:
                break
            key = byte & 0x1F
            data_type = byte >> 5
            metadata[key] = (data_type, type_dispatch[data_type](socket))
        return metadata

class ChunkData(BaseType):
    @classmethod
    def parse(cls, socket):
        data_size = Int.parse(socket)
        return zlib.decompress(recvall(socket, data_size))


class Packet(object):
    SERVER = 1
    CLIENT = 2
    BOTH = SERVER | CLIENT

    server_packet_types = {}
    client_packet_types = {}

    @classmethod
    def registered(cls, side, packet_id):
        def wrapper(packet_class):
            def register(register):
                try:
                    old_class = register[packet_id]
                except KeyError:
                    pass
                else:
                    logging.warning(
                        'Overriding %s (%s) with %s',
                        old_class.__name__,
                        hex(packet_id),
                        packet_class.__name__
                    )
                register[packet_id] = packet_class

            if side & cls.SERVER:
                register(cls.server_packet_types)
            if side & cls.CLIENT:
                register(cls.client_packet_types)

            packet_class.packet_id = packet_id

            return packet_class
        return wrapper

    @classmethod
    def parse(cls, register, socket):
        packet_id = UnsignedByte.parse(socket)
        try:
            packet_class = register[packet_id]
        except KeyError:
            raise ProtocolError('Unhandled packet type: %s' % hex(packet_id))
        packet = packet_class()
        for name, field_type in packet.fields:
            setattr(packet, name, field_type.parse(socket))
        return packet

    @classmethod
    def parse_client(cls, socket):
        return cls.parse(cls.client_packet_types, socket)

    @classmethod
    def parse_server(cls, socket):
        return cls.parse(cls.server_packet_types, socket)

    def __init__(self, **kwargs):
        for key, value in kwargs.iteritems():
            setattr(self, key, value)

    def serialize(self):
        return (
            UnsignedByte.serialize(self.packet_id) +
            ''.join(
                field_type.serialize(getattr(self, field))
                for field, field_type in self.fields
            )
        )

    def repr_field(self, key, value):
        return value

    def __repr__(self):
        return '<%s: %s>' % (
            self.__class__.__name__,
            ', '.join(
                '%s=%s' % (key, self.repr_field(key, getattr(self, key)))
                for key, field_type in self.fields
            )
        )

@Packet.registered(Packet.BOTH, 0x00)
class KeepAlive(Packet):
    fields = (
        ('id', Int),
    )

@Packet.registered(Packet.CLIENT, 0x01)
class LoginRequestClient(Packet):
    fields = (
        ('protocol_version', Int),
        ('username', String),
        ('_unused1', Long),
        ('_unused2', String),
        ('_unused3', Int),
        ('_unused4', Byte),
        ('_unused5', Byte),
        ('_unused6', UnsignedByte),
        ('_unused7', UnsignedByte),
    )

    def __init__(self, **kwargs):
        self._unused1 = 0
        self._unused2 = ''
        self._unused3 = 0
        self._unused4 = 0
        self._unused5 = 0
        self._unused6 = 0
        self._unused7 = 0
        super(LoginRequestClient, self).__init__(**kwargs)

@Packet.registered(Packet.SERVER, 0x01)
class LoginRequestServer(Packet):
    fields = (
        ('entity_id', Int),
        ('_unused1', String),
        ('map_seed', Long),
        ('level_type', String),
        ('server_mode', Int),
        ('dimension', Byte),
        ('difficulty', Byte),
        ('world_height', UnsignedByte),
        ('max_players', UnsignedByte),
    )

@Packet.registered(Packet.CLIENT, 0x02)
class HandshakeClient(Packet):
    fields = (
        ('username', String),
    )

@Packet.registered(Packet.SERVER, 0x02)
class HandshakeServer(Packet):
    fields = (
        ('connection_hash', String),
    )

@Packet.registered(Packet.BOTH, 0x03)
class ChatMessage(Packet):
    fields = (
        ('message', String),
    )

@Packet.registered(Packet.SERVER, 0x04)
class TimeUpdate(Packet):
    fields = (
        ('time', Long),
    )

@Packet.registered(Packet.BOTH, 0x05)
class EntityEquipment(Packet):
    fields = (
        ('entity_id', Int),
        ('slot', Short),
        ('item_id', Short),
        ('damage', Short),
    )

@Packet.registered(Packet.SERVER, 0x06)
class SpawnPosition(Packet):
    fields = (
        ('x', Int),
        ('y', Int),
        ('z', Int),
    )

@Packet.registered(Packet.CLIENT, 0x07)
class UseEntity(Packet):
    fields = (
        ('user', Int),
        ('target', Int),
        ('left_click', Bool),
    )

@Packet.registered(Packet.SERVER, 0x08)
class UpdateHealth(Packet):
    fields = (
        ('health', Short),
        ('food', Short),
        ('food_saturation', Float),
    )

@Packet.registered(Packet.BOTH, 0x09)
class Respawn(Packet):
    fields = (
        ('dimension', Byte),
        ('difficulty', Byte),
        ('creative_mode', Byte),
        ('world_height', Short),
        ('map_seed', Long),
        ('level_type', String),
    )

@Packet.registered(Packet.CLIENT, 0x0A)
class Player(Packet):
    fields = (
        ('on_ground', Bool),
    )

@Packet.registered(Packet.CLIENT, 0x0B)
class PlayerPosition(Packet):
    fields = (
        ('x', Double),
        ('y', Double),
        ('stance', Double),
        ('z', Double),
        ('on_ground', Bool),
    )

@Packet.registered(Packet.CLIENT, 0x0C)
class PlayerLook(Packet):
    fields = (
        ('yaw', Float),
        ('pitch', Float),
        ('on_ground', Bool),
    )

@Packet.registered(Packet.CLIENT, 0x0D)
class PlayerPositionLookClient(Packet):
    fields = (
        ('x', Double),
        ('y', Double),
        ('stance', Double),
        ('z', Double),
        ('yaw', Float),
        ('pitch', Float),
        ('on_ground', Bool),
    )

@Packet.registered(Packet.SERVER, 0x0D)
class PlayerPositionLookServer(Packet):
    fields = (
        ('x', Double),
        ('stance', Double),
        ('y', Double),
        ('z', Double),
        ('yaw', Float),
        ('pitch', Float),
        ('on_ground', Bool),
    )

@Packet.registered(Packet.BOTH, 0x0E)
class PlayerDigging(Packet):
    fields = (
        ('status', Byte),
        ('x', Int),
        ('y', Byte),
        ('z', Int),
        ('face', Byte),
    )

@Packet.registered(Packet.CLIENT, 0x0F)
class PlayerBlockPlacement(Packet):
    fields = (
        ('x', Int),
        ('y', Byte),
        ('z', Int),
        ('direction', Byte),
        ('held_item', Slot),
    )

@Packet.registered(Packet.CLIENT, 0x10)
class PlayerBlockPlacement(Packet):
    fields = (
        ('slot_id', Short),
    )

@Packet.registered(Packet.SERVER, 0x11)
class PlayerBlockPlacement(Packet):
    fields = (
        ('entity_id', Int),
        ('in_bed', Byte),
        ('head_x', Int),
        ('head_y', Byte),
        ('head_z', Int),
    )

@Packet.registered(Packet.BOTH, 0x12)
class Animation(Packet):
    fields = (
        ('entity_id', Int),
        ('animation', Byte),
    )

@Packet.registered(Packet.CLIENT, 0x13)
class EntityAction(Packet):
    fields = (
        ('entity_id', Int),
        ('action_id', Byte),
    )

@Packet.registered(Packet.SERVER, 0x14)
class NamedEntitySpawn(Packet):
    fields = (
        ('entity_id', Int),
        ('player_name', String),
        ('x', Int),
        ('y', Int),
        ('z', Int),
        ('rotation', Byte),
        ('pitch', Byte),
        ('current_item', Short),
    )

@Packet.registered(Packet.SERVER, 0x15)
class PickupSpawn(Packet):
    fields = (
        ('entity_id', Int),
        ('item', Short),
        ('count', Byte),
        ('data', Short),
        ('x', Int),
        ('y', Int),
        ('z', Int),
        ('rotation', Byte),
        ('pitch', Byte),
        ('roll', Byte),
    )

@Packet.registered(Packet.SERVER, 0x16)
class CollectedItem(Packet):
    fields = (
        ('entity_id', Int),
        ('collected_id', Int),
    )

@Packet.registered(Packet.SERVER, 0x17)
class AddObjectVehicle(Packet):
    fields = (
        ('entity_id', Int),
        ('type', Byte),
        ('x', Int),
        ('y', Int),
        ('z', Int),
        ('thrower_id', Int),
        ('speed_x', Short),
        ('speed_y', Short),
        ('speed_z', Short),
    )

@Packet.registered(Packet.SERVER, 0x18)
class MobSpawn(Packet):
    fields = (
        ('entity_id', Int),
        ('type', Byte),
        ('x', Int),
        ('y', Int),
        ('z', Int),
        ('yaw', Byte),
        ('pitch', Byte),
        ('metadata', Metadata),
    )

@Packet.registered(Packet.SERVER, 0x19)
class EntityPanting(Packet):
    fields = (
        ('entity_id', Int),
        ('title', String),
        ('x', Int),
        ('y', Int),
        ('z', Int),
        ('direction', Int),
    )

@Packet.registered(Packet.SERVER, 0x1a)
class ExperienceOrb(Packet):
    fields = (
        ('entity_id', Int),
        ('x', Int),
        ('y', Int),
        ('z', Int),
        ('count', Short),
    )

@Packet.registered(Packet.BOTH, 0x1C)
class EntityVelocity(Packet):
    fields = (
        ('entity_id', Int),
        ('velocity_x', Short),
        ('velocity_y', Short),
        ('velocity_z', Short),
    )

@Packet.registered(Packet.SERVER, 0x1D)
class DestroyEntity(Packet):
    fields = (
        ('entity_id', Int),
    )

@Packet.registered(Packet.BOTH, 0x1E)
class Entity(Packet):
    fields = (
        ('entity_id', Int),
    )

@Packet.registered(Packet.SERVER, 0x1F)
class EntityRelativeMove(Packet):
    fields = (
        ('entity_id', Int),
        ('dx', Byte),
        ('dy', Byte),
        ('dz', Byte),
    )

@Packet.registered(Packet.SERVER, 0x20)
class EntityLook(Packet):
    fields = (
        ('entity_id', Int),
        ('yaw', Byte),
        ('pitch', Byte),
    )

@Packet.registered(Packet.SERVER, 0x21)
class EntityLookRelativeMove(Packet):
    fields = (
        ('entity_id', Int),
        ('dx', Byte),
        ('dy', Byte),
        ('dz', Byte),
        ('yaw', Byte),
        ('pitch', Byte),
    )

@Packet.registered(Packet.SERVER, 0x22)
class EntityTeleport(Packet):
    fields = (
        ('entity_id', Int),
        ('x', Int),
        ('y', Int),
        ('z', Int),
        ('yaw', Byte),
        ('pitch', Byte),
    )

@Packet.registered(Packet.SERVER, 0x26)
class EntityStatus(Packet):
    fields = (
        ('entity_id', Int),
        ('entity_status', Byte),
    )

@Packet.registered(Packet.BOTH, 0x27)
class AttachEntity(Packet):
    fields = (
        ('entity_id', Int),
        ('vehicle_id', Int),
    )

@Packet.registered(Packet.SERVER, 0x28)
class EntityMetadata(Packet):
    fields = (
        ('entity_id', Int),
        ('metadata', Metadata),
    )

@Packet.registered(Packet.SERVER, 0x29)
class EntityEffect(Packet):
    fields = (
        ('entity_id', Int),
        ('effect_id', Byte),
        ('amplifier', Byte),
        ('duration', Short),
    )

@Packet.registered(Packet.BOTH, 0x2A)
class AttachEntity(Packet):
    fields = (
        ('entity_id', Int),
        ('effect_id', Byte),
    )

@Packet.registered(Packet.SERVER, 0x2B)
class Experience(Packet):
    fields = (
        ('experience_bar', Float),
        ('level', Short),
        ('total_experience', Short),
    )

@Packet.registered(Packet.SERVER, 0x32)
class PreChunk(Packet):
    fields = (
        ('x', Int),
        ('z', Int),
        ('mode', Bool),
    )

@Packet.registered(Packet.SERVER, 0x33)
class MapChunk(Packet):
    fields = (
        ('x', Int),
        ('y', Short),
        ('z', Int),
        ('size_x', Byte),
        ('size_y', Byte),
        ('size_z', Byte),
        ('data', ChunkData),
    )

    def repr_field(self, key, value):
        if key == 'data':
            return '… (%s bytes)' % len(value)
        else:
            return value

@Packet.registered(Packet.SERVER, 0x34)
class MultiBlockChange(Packet):
    fields = (
        ('x', Int),
        ('z', Int),
        ('data', MultiArray(
            Short, [
                ('coordinates', Short),
                ('types', Byte),
                ('metadata', Byte),
            ]
        )),
    )

    def repr_field(self, key, value):
        if key == 'data':
            return '… (%s items)' % len(value[0])
        else:
            return value

@Packet.registered(Packet.SERVER, 0x35)
class BlockChange(Packet):
    fields = (
        ('x', Int),
        ('y', Byte),
        ('z', Int),
        ('block_type', Byte),
        ('block_metadata', Byte),
    )

@Packet.registered(Packet.SERVER, 0x36)
class BlockAction(Packet):
    fields = (
        ('x', Int),
        ('y', Short),
        ('z', Int),
        ('byte1', Byte),
        ('byte2', Byte),
    )

@Packet.registered(Packet.SERVER, 0x3C)
class Explosion(Packet):
    fields = (
        ('x', Double),
        ('y', Double),
        ('z', Double),
        ('_unknown', Float),
        ('records', MultiArray(
            Int, [
                ('x', Byte),
                ('y', Byte),
                ('z', Byte),
            ]
        ))
    )

@Packet.registered(Packet.SERVER, 0x3D)
class SoundEffect(Packet):
    fields = (
        ('effect_id', Int),
        ('x', Int),
        ('y', Byte),
        ('z', Int),
        ('data', Int),
    )

@Packet.registered(Packet.SERVER, 0x46)
class NewOrInvalidState(Packet):
    fields = (
        ('reason', Byte),
        ('game_mode', Byte),
    )

@Packet.registered(Packet.SERVER, 0x47)
class Thunderbolt(Packet):
    fields = (
        ('entity_id', Int),
        ('_unknown', Bool),
        ('x', Int),
        ('y', Int),
        ('z', Int)
    )

@Packet.registered(Packet.SERVER, 0x64)
class OpenWindow(Packet):
    fields = (
        ('window_id', Byte),
        ('inventory_type', Byte),
        ('window_title', String),
        ('slots_count', Byte),
    )

@Packet.registered(Packet.SERVER, 0x65)
class CloseWindow(Packet):
    fields = (
        ('window_id', Byte),
    )

@Packet.registered(Packet.CLIENT, 0x66)
class WindowClick(Packet):
    fields = (
        ('window_id', Byte),
        ('slot', Short),
        ('right_click', Byte),
        ('action_number', Short),
        ('shift', Bool),
        ('clicked_item', Slot),
    )

@Packet.registered(Packet.SERVER, 0x67)
class SetSlot(Packet):
    fields = (
        ('window_id', Byte),
        ('slot', Short),
        ('slot_data', Slot),
    )

@Packet.registered(Packet.SERVER, 0x68)
class WindowItems(Packet):
    fields = (
        ('window_id', Byte),
        ('slots', Array(Short, Slot)),
    )

@Packet.registered(Packet.SERVER, 0x69)
class UpdateWindowProperty(Packet):
    fields = (
        ('window_id', Byte),
        ('property', Short),
        ('value', Short),
    )

@Packet.registered(Packet.SERVER, 0x6A)
class Transaction(Packet):
    fields = (
        ('window_id', Byte),
        ('action_number', Short),
        ('accepted', Bool),
    )

@Packet.registered(Packet.SERVER, 0x6B)
class CreateInventoryAction(Packet):
    fields = (
        ('slot', Short),
        ('clicked_item', Slot),
    )

@Packet.registered(Packet.CLIENT, 0x6C)
class EnchantItem(Packet):
    fields = (
        ('window_id', Byte),
        ('enchantment', Byte),
    )

@Packet.registered(Packet.SERVER, 0x82)
class UpdateSign(Packet):
    fields = (
        ('x', Int),
        ('y', Short),
        ('z', Int),
        ('line1', String),
        ('line2', String),
        ('line3', String),
        ('line4', String),
    )

@Packet.registered(Packet.SERVER, 0x83)
class ItemData(Packet):
    fields = (
        ('item_type', Short),
        ('item_id', Short),
        ('text', Array(UnsignedByte, Byte)),
    )

@Packet.registered(Packet.SERVER, 0xC8)
class IncrementStatistic(Packet):
    fields = (
        ('statistic_id', Int),
        ('amount', Byte),
    )

@Packet.registered(Packet.SERVER, 0xC9)
class PlayerListItem(Packet):
    fields = (
        ('player_name', String),
        ('online', Bool),
        ('ping', Short),
    )

@Packet.registered(Packet.SERVER, 0xFA)
class PluginMessage(Packet):
    fields = (
        ('channel', String),
        ('data', Array(Short, Byte)),
    )

@Packet.registered(Packet.CLIENT, 0xFE)
class ServerListPing(Packet):
    fields = ()

@Packet.registered(Packet.BOTH, 0xFF)
class Disconnect(Packet):
    fields = (
        ('reason', String),
    )
