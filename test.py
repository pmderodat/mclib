# -*- coding: utf-8 -*-

import StringIO

import packet

class TestSocket(StringIO.StringIO):
    def __init__(self, *args, **kwargs):
        StringIO.StringIO.__init__(self, *args, **kwargs)
        self.recv = self.read

sock = TestSocket('\x00\x01\x02\x03\x04')
print 0x01020304
p = packet.Packet.parse_server(sock)
print p
print repr(p.serialize())
