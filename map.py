# -*- coding: utf-8 -*-

import collections
import math

from packet import PlayerPositionLookClient



Block = collections.namedtuple(
    'Block',
    ('type', 'metadata', 'light', 'sky_light')
)

empty_block = Block(0, 0, 0, 0)

class Chunk(object):

    WIDTH = 16
    HEIGHT = 128
    DEPTH = 16

    def __init__(self):
        self.blocks = [
            empty_block
            for i in xrange(self.WIDTH * self.HEIGHT * self.DEPTH)
        ]

    def get(self, coordinates):
        x, y, z = coordinates
        return self.blocks[
            y + (z * self.HEIGHT) + (x * self.HEIGHT * self.DEPTH)
        ]

    def put(self, coordinates, block):
        x, y, z = coordinates
        self.blocks[
            y + (z * self.HEIGHT) + (x * self.HEIGHT * self.DEPTH)
        ] = block



class Map(object):
    def __init__(self):
        self.chunks = {}

    def get(self, coordinates):
        try:
            return self.chunks[coordinates]
        except KeyError:
            return None

    def put(self, coordinates, chunk):
        self.chunks[coordinates] = chunk
        chunk.coordinates = coordinates

    def remove(self, coordinates):
        # If the server tells to remove a not allocated chunk, something *is*
        # wrong, hence we must raise an error.
        self.chunks.pop(coordinates)

class Player(object):
    def __init__(self, map):
        self.map = map
        self.x = 0
        self.y = 0
        self.z = 0
        self.stance = 0.11
        self.yaw = 0
        self.pitch = 0
        self.on_ground = False

    def set_position(self, x=None, y=None, z=None, stance=None):
        if x is not None:
            self.x = x
        if y is not None:
            self.y = y
        if z is not None:
            self.z = z
        if stance is not None:
            self.stance = stance
        else:
            self.stance = self.y + 0.5

    def set_look(self, yaw, pitch):
        self.yaw, self.pitch = yaw, pitch

    def get_chunk(self):
        return self.map.get((int(self.x) >> 4, int(self.z) >> 4))

    def get_position_packet(self):
        return PlayerPositionLookClient(
            x=self.x, y=self.y, z=self.z,
            stance=self.stance, on_ground=self.on_ground,
            yaw=self.yaw, pitch=self.pitch
        )

    def center(self):
        '''
        Put the player at the center of the current block.
        '''
        self.set_position(
            x=math.floor(self.x) + 0.5,
            z=math.floor(self.z) + 0.5
        )
